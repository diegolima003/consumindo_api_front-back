const express = require('express')
const cors = require('cors')
const axios = require('axios')

const app = express()

app.use(cors()) // com o cors conseguimos liberar a nossa api para qualquer outro servidor



app.get('/', async(req, res)=>{

    const { data } = await axios('https://jsonplaceholder.typicode.com/users')
    //console.log(dados)


    return res.json(data)
})


const port = 2000
app.listen(port)